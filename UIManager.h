#ifndef UIMANAGER_H
#define UIMANAGER_H
#include <string>

class Boundary {
public:
	float left, right, top, bottom;

	void boundary();
};

class Screens {
public:

	void start_screen();
	void game_end_screen();
	void win_screen();
	
	GLuint startID, endID, winID, backgroundID, background2ID, background3ID, background4ID, background5ID;
};

class Texts {
public:
	void text();
	void printtext(float x, float y, std::string String);
};

#endif