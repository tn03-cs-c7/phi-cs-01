#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H

class Image {
public:
	Image(char* ps, int w, int h);
	~Image();

	char* pixels;
	int width;
	int height;
};

//Reads a bitmap image from file.
Image* loadBMP(const char* filename);

class Sounds {
public:

	void soundBrick();
	void soundPad();
	void powerUpSound();
	void lifeLostSound();
	void GameEndSound();
};

class Textures {
public:
	void loadTexture(const char* filename, GLuint& ID);
};
#endif