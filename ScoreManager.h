#ifndef SCOREMANAGER_H
#define SCOREMANAGER_H
#include <string>

class Score {
public:
	void PrintScreen(float x, float y, std::string String);
	void WinScreenMessage();
	void Game_End_ScreenMessage();
};
#endif

