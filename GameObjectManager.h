#ifndef GAMEOBJECTMANAGER_H
#define GAMEOBJECTMANAGER_H
#include <glut.h> 

class Brick {
public:
	float x, y;   // center of the brick
	float height, width; // dimensions
	int life;  // life of the brick;
	void brick();
	void set_level();

	GLuint brickID, brick2ID, brick3ID, brick4ID, brick5ID;
};

class Pad {
public:
	Pad() { height = 0.12; width = 1.0; }
	void setPosition(double X, double Y) { x = X; y = Y; }

public:
	float x, y;          // center of the paddle
	float height, width; // dimensions
	GLuint padID;
};

class Ball {
public:
	float r;       // radius
	float x, y;  // centre

	float vx, vy; // velocity

	void moveBall();

	GLuint ballID;
};


class Powers {
public:

	void PowerUps();
	void PowerTimer();
	
	Powers() { powerWH = 0.18; } //Defined width/height of powerup square

	float powerPadX, powerBallX, powerLifeX;  //x position of power up
	float powerPadY, powerBallY, powerLifeY; //y position of power up

	float power_vy; // Y Velocity of power up (Only Y needed as the power up only moves vertically)

	GLuint starID, arrowID, thunderID;

public:
	float powerWH;
};

class Reset {
public:
	void reset();
};

class Time {
public:
	static void Timer(int value);
};
#endif


