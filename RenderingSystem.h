#ifndef RENDERINGSYSTEM_H
#define RENDERINGSYSTEM_H

class Renderer
{
public:
	
	~Renderer() {};
	
public:
	void DrawRectangle(float x, float y, float width, float height);
	void DrawCircle(float x, float y, float radius);
	void DrawImage(float x, float y, const Image* image);
	
protected:
	Renderer() {};
};


class Renderable 
{
public:
	inline int ZOrder() const { return m_zorder; }
	inline int ZOrder(int value) { m_zorder=value; }


	virtual ~Renderable() {};
	virtual void Draw(const Renderer *render)=0;

protected:
	Renderable() :m_zorder{0} {};
private:
	int m_zorder;
};

class Collidable
{
public:
	virtual ~Collidable();
	virtual bool Collinding(float x, float y, float width, float height);

protected:
	Collidable();
};

class Ball : public Renderable, public Collidable
{
public:
	Ball(float radius);
	void Draw(Renderer* renderer) const
	{
		if (renderer)
		{
			renderer->DrawCircle(X, Y, Radius);
		}
	}
	float X;
	float Y;
	float Radius;
	explicit Ball(Image* image): Renderable()
	{
		ZOrder(-10);
	}
	
};

class Brick : public Renderable, public Collidable
{
public:
	Brick(float x,float y);
	void Draw(Renderer* renderer) const
	{
		if (renderer)
		{
			renderer->DrawRectangle(X, Y, Width, Height);
		}
	}
	float X;
	float Y;
	float Width;
	float Height;
	explicit Brick(Image* image) : Renderable()
	{
		ZOrder(-10);
	}

};

class Pad : public Renderable, public Collidable
{
public:
	Pad(float x, float y);
	void Draw(Renderer* renderer) const
	{
		if (renderer)
		{
			renderer->DrawRectangle(X, Y, Width, Height);
		}
	}
	float X;
	float Y;
	float Width;
	float Height;
	explicit Pad(Image* image) : Renderable()
	{
		ZOrder(-10);
	}

};

class Background : public Renderable
{
public:
	Background(float x, float y);
	void Draw(Renderer* renderer) const
	{
		if (renderer)
		{
			renderer->DrawRectangle(X, Y, Width, Height);
		}
	}
	float X;
	float Y;
	float Width;
	float Height;
	explicit Background(Image* image) : Renderable()
	{
		ZOrder(-100);
	}

};

class Image {
public:
	Image(char* ps, int w, int h);
	~Image();

	char* pixels;
	int width;
	int height;
};
#endif
