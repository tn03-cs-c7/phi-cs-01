#ifndef COLLISIONMANAGER_H
#define COLLISIONMANAGER_H


class Collision {
public:
	void brickCollision();
	void padCollision();
	void powerCollision();
};
#endif